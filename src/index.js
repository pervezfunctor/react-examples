import Hello from './hello';
import LikeButton from './interactive_components';
import Counter from './counter.js';
import {FilterableProductTable, products} from './thinking_in_react.js';
import Avatar from './multiple_components.js';

import {RefsExample, FormsExample, KeysExample, FancyCheckbox} from './misc_examples.js';

import React from 'react';
import { render } from 'react-dom';

render(<KeysExample results={[ {id : 1, text : 'hello'}, {id : 2, text : 'world'}, {id: 3, text : 'universe'}]} />,
       document.getElementById("root"));
