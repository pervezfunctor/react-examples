import React, { Component } from 'react';

export default class LikeButton extends Component {
  constructor(props) {
    super(props);
    this.state = {liked: false};
  }

  handleClick(event) {
    this.setState({liked: !this.state.liked});
  }

  render() {
    var text = this.state.liked ? 'like' : 'haven\'t liked';
    return (
        <p onClick={ (evt) => this.handleClick(evt) }>
          You {text} this. Click to toggle.
        </p>
    );
  }
}
