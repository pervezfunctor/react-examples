import React, { Component } from 'react';

export class ProductCategoryRow extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <tr>
        <th colSpan="2">{this.props.category}</th>
        </tr>
    );
  }
}

export class ProductRow extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var name = this.props.product.stocked ?
          this.props.product.name :
          <span style={{color: 'red'}}>{this.props.product.name}</span>;
    return (
        <tr>
        <td>{name}</td>
        <td>{this.props.product.price}</td>
        </tr>
    );
  }
}

export class ProductTable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var rows = [];
    var lastCategory = null;
    this.props.products.forEach((product) => {
      if (product.name.indexOf(this.props.filterText) === -1 || (!product.stocked && this.props.inStockOnly))
        return;

      if (product.category !== lastCategory)
        rows.push(<ProductCategoryRow category={product.category} key={product.category} />);

      rows.push(<ProductRow product={product} key={product.name} />);
      lastCategory = product.category;
    });

    return (
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
    );
  }
}

export class SearchBar extends Component {
  constructor(props) {
    super(props);
  }

  handleChange() {
    this.props.onUserInput(
      this.refs.filterTextInput.value,
      this.refs.inStockOnlyInput.checked
    );
  }

  render() {
    return (
        <form>
          <input
            type="text"
            placeholder="Search..."
            value={this.props.filterText}
            ref="filterTextInput"
            onChange={(evt) => this.handleChange(evt)}
          />
          <p>
            <input
              type="checkbox"
              checked={this.props.inStockOnly}
              ref="inStockOnlyInput"
              onChange={(evt) => this.handleChange(evt)}
            />
            {' '}
            Only show products in stock
          </p>
        </form>
    );
  }
}

export class FilterableProductTable extends Component {
  constructor(props) {
    super(props);
    this.state = { filterText: '', inStockOnly: false };
  }

  handleUserInput(filterText, inStockOnly) {
    this.setState({ filterText: filterText, inStockOnly: inStockOnly });
  }

  render() {
    return (
        <div>
          <SearchBar
            filterText={this.state.filterText}
            inStockOnly={this.state.inStockOnly}
            onUserInput={(evt) => this.handleUserInput(evt)}
          />
          <ProductTable
            products={this.props.products}
            filterText={this.state.filterText}
            inStockOnly={(evt) => this.state.inStockOnly(evt)}
          />
        </div>
    );
  }
}

export let products = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

// ReactDOM.render(
//     <FilterableProductTable products={PRODUCTS} />,
//     document.getElementById('container')
// );
