import React, { Component } from 'react';


export class RefsExample extends Component {
  componentDidMount() {
    this.refs.second.focus();
  }

  render() {
    return (
        <div>
          <input ref='first' type='text' />
          <br />
          <input ref='second' type='text' />
        </div>
    );
  }
}

export class FormsExample extends Component {
  handleChange() {
    console.log(this.refs.textBox.value);
    console.log(this.refs.textArea.value);
    console.log(this.refs.checkBox.checked);
    console.log(this.refs.selectBox.value);
  }

  render() {
    return (
        <div>
          <input type='text' ref='textBox' onChange={this.handleChange.bind(this)} />
          <br />
          <textarea ref='textArea' onChange={this.handleChange.bind(this)} />
          <br />
          <input type='checkbox' ref='checkBox' onChange={this.handleChange.bind(this)} />
          <br />
          <select ref='selectBox' value='B' onChange={this.handleChange.bind(this)}>
            <option value='A'>Apple</option>
            <option value='B'>Banana</option>
            <option value='C'>Cranberry</option>
          </select>
        </div>
    );
  }
}

export class ListItemWrapper extends Component {
  render() {
    return <li>{this.props.data.text}</li>;
  }
}

export class KeysExample extends Component {
  render() {
    return (
      <ul>
        {this.props.results.map((result) => <ListItemWrapper key={result.id} data={result} />)}
      </ul>
    );
  }
}
